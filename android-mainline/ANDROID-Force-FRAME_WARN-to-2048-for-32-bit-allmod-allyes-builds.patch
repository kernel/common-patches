From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Todd Kjos <tkjos@google.com>
Date: Fri, 22 Nov 2024 17:19:13 +0000
Subject: ANDROID: Force FRAME_WARN to 2048 for 32-bit allmod/allyes builds

There are many FRAME_WARN issues reported by kernelci for 32-bit
allmodconfig builds which also are present on allyesconfig builds.

Example:
fs/bcachefs/chardev.c:881:6: error: stack frame size (1080) exceeds limit (1024) in 'bch2_fs_ioctl'

Suppress these warnings by increasing FRAME_WARN to 2048 bytes for
allmodconfig and allyes config builds.

Bug: 379954362
Signed-off-by: Todd Kjos <tkjos@google.com>
Change-Id: I020e2ff88b1e68fa1e081ccf5c790838e16575ac
---
 lib/Kconfig.debug | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/lib/Kconfig.debug b/lib/Kconfig.debug
--- a/lib/Kconfig.debug
+++ b/lib/Kconfig.debug
@@ -438,10 +438,16 @@ config GDB_SCRIPTS
 
 endif # DEBUG_INFO
 
+config FORCE_FRAME_WARN_TO_2K
+	bool "Force FRAME_WARN to 2048 for 32-bit allmod/allyes builds"
+	default n
+	depends on !64BIT
+
 config FRAME_WARN
 	int "Warn for stack frames larger than"
 	range 0 8192
 	default 0 if KMSAN
+	default 2048 if FORCE_FRAME_WARN_TO_2K
 	default 2048 if GCC_PLUGIN_LATENT_ENTROPY
 	default 2048 if PARISC
 	default 1536 if (!64BIT && XTENSA)
